<?php

namespace App\Http\Controllers;


use App\Product;
use App\Category;
use App\ProductCategory;

use Illuminate\Http\Request;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function get_categories() {
    	$categories = Category::latest()->get();
        $products = Product::orderBy('product_name')->get();


        return view( 'categories.index', compact('categories', 'products') );
    }

    public function get_category( $category_id ) {
    	$category   = Category::findOrFail( $category_id );
        $categories = Category::latest()->get();

        //get all products
        $products = Product::orderBy('product_name')->get();

        //get all product ids
        $product_ids   = ProductCategory::where('category_id', $category_id)->lists('product_id')->toArray();
        $product_infos = array();

        foreach( $product_ids as $product_id ){
            $product_info =  Product::findOrFail($product_id)->toArray();
            array_push( $product_infos, $product_info );
        }

        return view( 'categories.view_category', compact('category', 'categories', 'products', 'product_ids', 'product_infos' ) );
    }

    public function create() {
        $products = Product::orderBy('product_name')->get();

        return view( 'categories.create', compact('products'));
    }

     public function store( CreateCategoryRequest $request) {

        $input       = $request->all();
        $category    = Category::create( $input );
        $category_id = $category->id;

        if( $request->has('product') ) {
            $products = $request->get('product');
            
            //save product categories
            ProductCategory::save_product_categories( $products, '', $category_id );
        }

       return redirect('categories');
    }

   public function update($id, CreateCategoryRequest $request) {
        $category = Category::findOrFail($id);

        $input = $request->all();

        $category->fill($input)->save();

        //delete old categories
        ProductCategory::delete_all_product_category( 'category_id', $id );

        //get selected categories
        $products = $request->get('product');
        
        //save new product categories
        ProductCategory::save_product_categories( $products, '', $id );

        return redirect()->back();
    }

    public function delete( $id ) {

        self::delete_category( $id );

        return redirect('categories');
    }

    public function delete_category( $id ) {
        $category = Category::findOrFail($id);

        ProductCategory::delete_all_product_category( 'category_id', $id );

        $category->delete();
    }

    public function delete_categories( CreateCategoryRequest $request ){

        $c_ids    = $request->get('categories');

        foreach ($c_ids as $c_id) {
            self::delete_category( $c_id );
        }

        return redirect()->back();
    }

    public function copy_move_product( $current_cat_id, CreateCategoryRequest $request ) {
        $input    = $request->all();
        $action   = $request->action;
        $to       = $request->category;
        $p_ids    = $request->get('c');

        if( $action == 'move' ){
            if( $to != 'select' ) {
                self::move_product( $p_ids, $current_cat_id, $to );
            }
        } else if ( $action == 'copy' ) {
            if( $to != 'select' ) {
                self::copy_product( $p_ids, $to );
            }
        } else if( $action == 'delete' ){
            foreach( $p_ids as $product_id ) {
                ProductCategory::delete_product_category( $product_id, $current_cat_id );
            }
        }
        
        return redirect()->back();
    }

    public function move_product( $p_ids, $old_category_id, $new_category_id ) {
        foreach( $p_ids as $p_id ) {
            ProductCategory::delete_product_category($p_id, $old_category_id);
        }
        ProductCategory::save_product_categories($p_ids, '', $new_category_id);

        return redirect()->back();
    }

    public function copy_product( $p_ids, $to ) {
        ProductCategory::save_product_categories( $p_ids, '', $to );

        return redirect()->back();
    }
}
