<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\ProductCategory;

//use Illuminate\Http\Request;
use Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Requests\CreateProductRequest;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{

	public function delete_pc( $pid, $cid ) {
		ProductCategory::delete_product_category( $pid, $cid );

		return redirect()->back();
	}
	    
}
