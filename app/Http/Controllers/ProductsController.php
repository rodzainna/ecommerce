<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\ProductCategory;

//use Illuminate\Http\Request;
use Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Requests\CreateProductRequest;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function get_products() {
    	$products   = Product::latest()->get();
        
        return view( 'products.index', compact('products') );
    }

    public function get_product( $product_id ) {
        //get product info
    	$product    = Product::findOrFail( $product_id );
        
        //get product info id
        //$pc_ids     = ProductCategory::get_pc_ids( 'product_id', $product_id );
        
        //get all categories
        $categories = Category::orderBy('category_name')->get();

        //get all category ids
        $cat_ids        = ProductCategory::where('product_id', $product_id)->lists('category_id')->toArray();
        $category_infos = array();

        foreach( $cat_ids as $cat_id ){
            $cat_info =  Category::findOrFail($cat_id)->toArray();
            array_push( $category_infos, $cat_info );
        }

        return view( 'products.view_product', compact('product', 'categories', 'cat_ids', 'category_infos' ) );
    }

    
    public function create() {
        $categories = Category::orderBy('category_name')->get();

    	return view( 'products.create', compact('categories'));
    }

    public function store( CreateProductRequest $request) {

        $input      = $request->all();
        $product    = Product::create( $input );

        $product_id = $product->id;

        if( $request->has('cat')) {
            $categories = $request->get('cat');
            
            //save product categories
            ProductCategory::save_product_categories( $categories, $product_id, '' );
        }

       return redirect('products');
    }

   public function update($product_id, CreateProductRequest $request) {
        $product = Product::findOrFail($product_id);

        $input   = $request->all();

        //update product
        $product->fill($input)->save();

        //delete old categories
        ProductCategory::delete_all_product_category( 'product_id', $product_id );

        //get selected categories
        $categories = $request->get('cat');

         //find if product_id and category_id exists
        //$new_categories = self::new_categories( $product_id, $categories );
        
        //save new product categories
        ProductCategory::save_product_categories( $categories, $product_id, '' );

        return redirect()->back();
    }

    public function delete( $id ) {

        self::delete_product( $id );

        return redirect('products');
    }

    public function delete_product( $id ) {
        $product = Product::findOrFail($id);

        ProductCategory::delete_all_product_category( 'product_id', $id );

        $product->delete();
    }


    public function delete_products( CreateProductRequest $request ){

        $p_ids    = $request->get('products');

        foreach ($p_ids as $p_id) {
            self::delete_product( $p_id );
        }

        return redirect()->back();
    }
}
