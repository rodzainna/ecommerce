<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

//Route::resource('products', 'ProductsController');

Route::get('products', 'ProductsController@get_products');
Route::get('product/create', 'ProductsController@create');
Route::get('product/{id}', 'ProductsController@get_product');

Route::post('product', 'ProductsController@store');
Route::post('delete-products', 'ProductsController@delete_products');
Route::post('update-product/{id}', 'ProductsController@update');
Route::post('delete-product/{id}', 'ProductsController@delete');

Route::get('categories', 'CategoryController@get_categories');
Route::get('category/create', 'CategoryController@create');
Route::get('category/{id}', 'CategoryController@get_category');

Route::post('category', 'CategoryController@store');
Route::post('delete-categories', 'CategoryController@delete_categories');
Route::post('update-category/{id}', 'CategoryController@update');
Route::post('delete-category/{id}', 'CategoryController@delete');
Route::post('copy-move-product/{id}', 'CategoryController@copy_move_product');

Route::get('delete-pc/pid={pid}&cid={cid}', 'ProductCategoryController@delete_pc');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    
});
