<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    	'sku',
    	'product_name',
    	'product_desc',
    	'model',
    	'size',
    	'unit_price'
    ];

    public function scopePublished( $query ) {
    	$query->where('published_at', '<=', Carbon::now());
    }

    public function scopeUnublished( $query ) {
    	$query->where('published_at', '>=', Carbon::now());
    }
}
