<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent;

class ProductCategory extends Model
{
    protected $fillable = [
    	'product_id',
    	'category_id'
    ];

    public static function get_pc_ids( $slug, $id ) {
        //get product category ids
        $pc_ids  = ProductCategory::where($slug, $id)->lists('id')->toArray();

        return $pc_ids;
    }

    public static function check_if_pc_exists ( $product_id, $category_id ) {       
        $pc_id = ProductCategory::where('product_id', $product_id)->where('category_id', $category_id)->lists('id')->toArray();

        if( empty ( $pc_id ) ){
            return false;
        } else {
            return true;
        }
    }

    public static function save_product_categories( $ids, $product_id, $category_id ){
    	if( is_array( $ids ) )
        {
            foreach( $ids as $id ) {
                if ( !empty( $product_id ) ) {  
                    //var_dump( self::check_if_pc_exists( $product_id, $id ) );
                    if( self::check_if_pc_exists( $product_id, $id ) == false ) {
    	                $pc  = array(
    	                	'product_id'  => $product_id,
    	                	'category_id' => $id
    	                );

                        ProductCategory::create( $pc );
                    } //check if pc exists
	            } else if( !empty( $category_id ) ) {
                    //var_dump( self::check_if_pc_exists( $id, $category_id ) );
                    if( self::check_if_pc_exists( $id, $category_id ) == false ) {
    	                $pc  = array(
    	                	'product_id'  => $id,
    	                	'category_id' => $category_id
    	                );
                        ProductCategory::create( $pc );
                    } //check if pc exists
                } //if not empty category_id
            } //foreach ids as id
        } //if ids is array
    }

    public static function delete_all_product_category( $slug, $id ) {
        $pc_ids = self::get_pc_ids( $slug, $id );

        foreach( $pc_ids as $pc_id ) {
            $pc = ProductCategory::findOrFail( $pc_id );

            $pc->delete();
        }
    }

    public static function delete_product_category( $product_id, $category_id ) {
        $pc_id = ProductCategory::where('product_id', $product_id)->where('category_id', $category_id)->lists('id');
        $pc    = ProductCategory::findOrFail( $pc_id );
        $pc->delete();
    }
}
