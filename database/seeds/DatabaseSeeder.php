<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        Model::unguard();

	    $this->call(ProductTableSeeder::class);
	    $this->call(CategoryTableSeeder::class);
	    $this->call(ProductCategoryTableSeeder::class);
        $this->command->info("Users table seeded :)");
	    Model::reguard();
        */

        //delete product table records
        DB::table('products')->delete();
        //insert some dummy records
        DB::table('products')->insert(array(
           array(
            'sku'         =>'00000ZC451CG',
            'product_name'=>'ASUS Zenfone C',
            'product_desc'=>'Released 2015, February. Optional Dual SIM -- Micro-SIM, dual stand-by.',
            'model'       =>'ZC451CG',
            'size'        =>'5.37 x 2.64 x 0.43 in',
            'unit_price'  =>'4500.00'
            ),
           array(
            'sku'         =>'00003310',
            'product_name'=>'Nokia 3310',
            'product_desc'=>'The Nokia 3310 is a GSM mobile phone announced on September 1, 2000, and released in the fourth quarter of the year, replacing the popular Nokia 3210.',
            'model'       =>'3310',
            'size'        =>'4.45 in × 1.89 in × 0.87 in',
            'unit_price'  =>'900.00'
           ),
           array(
            'sku'         =>'0000A1000',
            'product_name'=>'Lenovo A1000',
            'product_desc'=>'At less than an inch thin and only weighing 0.7 lbs the Lenovo A1000 is a perfect size for reading, listening to music, and web-surfing – and it can easily slide into a pocket when not in use.',
            'model'       =>'A1000',
            'size'        =>'7 x 4.64 x 0.43 in',
            'unit_price'  =>'5000.00'
            ),
           array(
            'sku'         =>'0000EOS5D',
            'product_name'=>'Canon EOS 5D Mark III',
            'product_desc'=>'The Canon EOS 5D Mark III is a professional full-frame digital single-lens reflex (DSLR) camera made by Canon. It has a 22.3 megapixel CMOS image sensor.',
            'model'       =>'EOS 5D Mark III',
            'size'        =>'7 x 7 x 5 in',
            'unit_price'  =>'30000'
            )
        ));

        
         //delete category table records
        DB::table('categories')->delete();
        //insert some dummy records
        DB::table('categories')->insert(array(
           array(
            'category_name'=>'Smartphone',
            'category_desc'=>'A smartphone is a mobile phone with an advanced mobile operating system which combines features of a personal computer operating system with other features useful for mobile or handheld use.'
            ),
           array(
            'category_name'=>'Basic Phone',
            'category_desc'=>'A basic phone is a telephone that can make and receive calls over a radio frequency carrier while the user is moving within a telephone service area.'
            ),
           array(
            'category_name'=>'Tablet',
            'category_desc'=>'A tablet computer, commonly shortened to tablet, is a mobile computer with a touchscreen display, circuitry, and battery in a single device.'
            ),
           array(
            'category_name'=>'Camera',
            'category_desc'=>'A camera is an optical instrument for recording images, which may be stored locally, transmitted to another location, or both.'
            )
        ));


        //delete product_category table records
        DB::table('product_categories')->delete();
        //insert some dummy records
        DB::table('product_categories')->insert(array(
           array(
            'product_id' =>'1',
            'category_id'=>'1'
            ),
           array(
            'product_id' =>'2',
            'category_id'=>'2'
            ),
           array(
            'product_id' =>'3',
            'category_id'=>'3'
            ),
           array(
            'product_id' =>'4',
            'category_id'=>'4'
            )
        ));

    }
}
