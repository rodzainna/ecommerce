<!DOCTYPE html>

<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ecommerce App</title>

	<!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="{{ asset('css/morris.css') }}"  rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
    
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header"  >
                <a class="navbar-brand" href="{{ url( '/' ) }}" style="color: #FFFFFF">AppName</a>
                <a class="navbar-brand" href="{{ url( 'products' ) }}" style="color: #FFFFFF">Products</a>
                <a class="navbar-brand" href="{{ url( 'categories' ) }}" style="color: #FFFFFF">Categories</a>
            </div>
        </nav>
    	<div id="page-wrapper">
            <div class="container-fluid">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        @yield('content')
                    </div>
                </div>
            </div>
    	</div>
    

	@yield('footer')

     <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/plugins/morris/raphael.min.js') }}"></script>
    <script src="{{ asset('js/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ asset('js/plugins/morris/morris-data.js') }}"></script>

    <script type="text/javascript">
    function checkAll() {
        var is_checked = $('.selectAll:checked').val();

        if(is_checked === 'on') { // check select status
            $('.checkbox').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    }

    function productsCheckAll() {
        var is_checked = $('.selectProducts:checked').val();

        if(is_checked === 'on') { // check select status
            $('.checkbox-inline').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $('.checkbox-inline').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    }
    </script>
</body>
</html>