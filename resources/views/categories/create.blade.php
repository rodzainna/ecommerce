@extends('app')

@section('content')
<center>
	<h1>Add Category</h1>

	{!! Form::open(['url' => 'category']) !!}
		<div class="form-group">
			{!! Form::label( 'category_name', 'Category Name:' ) !!}
			{!! Form::text( 'category_name', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'category_desc', 'Description: ' ) !!}
			{!! Form::textarea( 'category_desc', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'product_category', 'Product(s): ' ) !!}
			<a href="#product-checkbox" data-toggle="collapse">See All Product(s)</a>
			<br>
			<div id="product-checkbox" class="collapse">
				@foreach( $products as $product )
					{!! Form::label( 'product['.$product->id.']', ''.$product->product_name.'', ['class="checkbox-inline"']  ) !!}
					{!! Form::checkbox( 'product['.$product->id.']', ''.$product->id.'' ) !!}		
				@endforeach
			</div>
		</div>

		{!! Form::submit( 'Add Category', ['class="btn btn-primary"'] ) !!}

	{!! Form::close() !!}
</center>
@stop