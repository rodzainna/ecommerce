@extends('app')

@section('content')
	<h1>Categories</h1>
	<a href="{{ url( '/category/create' ) }}" ><input type="button" class="btn btn-primary" name="create-category" id="create-category" value="Add Category" /></a>

	<hr>
	{!! Form::open(['url' => 'delete-categories']) !!}
		<input type="submit" class="btn btn-danger btn-xs" value="Delete" /><br/>
		<table class="table table-bordered table-hover table-striped">
			<thead>
				<th><center><input onclick="checkAll()" class="selectAll" type="checkbox" /></center></th>
				<th>Category Name</th>
				<th>Category Description</th>
			</thead>
			<tbody>	
				@foreach ( $categories as $category )
					<tr>
						<th><center><input type="checkbox" name="categories['<?php echo $category->id ?>']" class="checkbox" value="<?php echo $category->id ?>" /></center></th>
						<td><a href="{{ url( '/category', $category->id ) }}" ><div>{{ $category->category_name }}</div></a></td>
						<td><div>{{ $category->category_desc }}</div></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	{!! Form::close() !!}

@stop