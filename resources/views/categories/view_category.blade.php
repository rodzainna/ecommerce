@extends('app')

@section('content')
	<h2>Category</h2>
	<button type="submit" data-toggle="modal" data-target="#update-category" class="btn btn-sm btn-primary">Edit</button>
	<button type="submit" data-toggle="modal" data-target="#delete-category" class="btn btn-sm btn-danger">Delete</button>
	

	<h3>{{ $category->category_name }}</h3>

	<category>
		<div><strong>Description: </strong>{{ $category->category_desc }}</div>

		<hr>
		{!! Form::open(['url' => 'copy-move-product/'.$category->id.'']) !!}
			<select name="action">
            <option value="select">Select ..</option>
				<option value="move">Move to</option>
				<option value="copy">Copy to</option>
				<option value="delete">Delete</option>
			</select>

			<select name="category">
				<option value="select">Select ..</option>
				@foreach( $categories as $cat )
					@if( $cat->id != $category->id)
					<option value="<?php echo $cat->id ?>"><?php echo $cat->category_name ?></option>
					@endif
				@endforeach
			</select>

			<input type="submit" class="btn btn-primary btn-xs" value="Submit" />

			<div><strong>Product(s): </strong></div>

			<!--Start Table Products for this category-->
			<table class="table table-bordered table-hover table-striped">
				<thead>
					<th><center><input onclick="checkAll()" class="selectAll" type="checkbox" /></center></th>
					<th><center>Product Name</center></th>
					<th><center>SKU</center></th>
					<th><center>Model</center></th>
					<th><center>Size</center></th>
					<th><center>Price</center></th>
				</thead>
				<tbody>	
				@foreach( $product_infos as $p_info )
				<tr>
					<th><center><input type="checkbox" name="c['<?php echo $p_info['id'] ?>']" class="checkbox" value="<?php echo $p_info['id'] ?>" /></center></th>
					<td><a href="{{ url( '/product', $p_info['id'] ) }}" ><div>{{ $p_info['product_name'] }}</div></a></td>
					<td><div>{{ $p_info['sku'] }}</div></td>
					<td><div>{{ $p_info['model'] }}</div></td>
					<td><div>{{ $p_info['size'] }}</div></td>
					<td><div>{{ $p_info['unit_price'] }}</div></td>
				</tr>
				@endforeach
				</tbody>
			</table>
			<!--End table-->

		{!! Form::close() !!}
	</category>

	<!--Start Edit Category Modal-->
	<div id="update-category" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="text-align: center;  background-color: #337ab7; ">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 style="color: #ffffff; " class="modal-title">Edit Category Info</h4>
				</div>
			
				{!! Form::open(['url' => 'update-category/'.$category->id.'']) !!}
					<div class="col-lg-12 panel">
						<div class="panel-body">
							<div class="form-group">
								{!! Form::label( 'category_name', 'Category Name:' ) !!}
								{!! Form::text( 'category_name', ''.$category->category_name.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'category_desc', 'Description: ' ) !!}
								{!! Form::textarea( 'category_desc', ''.$category->category_desc.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'product_category', 'Product(s): ' ) !!}
								<a href="#product-checkbox" data-toggle="collapse">See All Product(s)</a>
								<br>
								<div id="product-checkbox" class="collapse">
									@foreach( $products as $product )
										<?php $value = false; ?>
										@if( in_array( $product->id, $product_ids ) )
											<?php $value = true; ?>
										@endif					
										{!! Form::label( 'product['.$product->id.']', ''.$product->product_name.'', ['class="checkbox-inline"']  ) !!}
										{!! Form::checkbox( 'product['.$product->id.']', ''.$product->id.'', $value ) !!}		
									@endforeach
								</div>
							</div>

							{!! Form::submit( 'Update Category', ['class="btn btn-block btn-primary"'] ) !!}
						</div>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- End edit Category Modal-->

	<!--Start Delete Category Modal-->
	<div id="delete-category" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="text-align: center; background-color: #337ab7; ">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 style="color: #ffffff; " class="modal-title">Delete Category</h4>
				</div>
				{!! Form::open(['url' => 'delete-category/'.$category->id.'']) !!}
					<div class="col-lg-12 panel">
						<div class="panel-body">
							<div class="form-group">
								<label>Are you sure you want to delete this category?</label>
							</div>
							<div class="form-group">
								{!! Form::submit( 'Delete Category', ['class="btn btn-danger"'] ) !!}
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- End Delete Category Modal-->

@stop