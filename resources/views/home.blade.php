@extends('app')

@section('content')
	
	<a href="{{ url( '/products' ) }}" ><button type="submit" data-toggle="modal" data-target="#update-product" class="btn btn-lg btn-success">Products</button></a>
	<a href="{{ url( '/categories' ) }}" ><button type="submit" data-toggle="modal" data-target="#delete-product" class="btn btn-lg btn-info">Categories</button></a>

@stop