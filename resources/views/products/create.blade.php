@extends('app')

@section('content')
<center>
	<h1>Add Product</h1>

	{!! Form::open(['url' => 'product']) !!}

		<div class="form-group">
			{!! Form::label( 'sku', 'SKU:' ) !!}
			{!! Form::text( 'sku', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'product_name', 'Product Name:' ) !!}
			{!! Form::text( 'product_name', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'product_desc', 'Description: ' ) !!}
			{!! Form::textarea( 'product_desc', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'model', 'Model: ' ) !!}
			{!! Form::text( 'model', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'size', 'Size: ' ) !!}
			{!! Form::text( 'size', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'unit_price', 'Price: ' ) !!}
			{!! Form::text( 'unit_price', '', ['class="form-control"'] ) !!}
		</div>

		<div class="form-group">
			{!! Form::label( 'product_category', 'Category: ' ) !!}
			<a href="#category-checkbox" data-toggle="collapse">See All Category(ies)</a>

			<div id="category-checkbox" class="collapse">
				@foreach( $categories as $category )						
					{!! Form::label( 'cat['.$category->id.']', ''.$category->category_name.'', ['class="checkbox-inline"']  ) !!}
					{!! Form::checkbox( 'cat['.$category->id.']', ''.$category->id.'' ) !!}		
				@endforeach
			</div>
		</div>

		{!! Form::submit( 'Add Product', ['class="btn btn-primary"'] ) !!}

	{!! Form::close() !!}
</center>
@stop