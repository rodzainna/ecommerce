@extends('app')

@section('content')
	<h1>Products</h1>
	<a href="{{ url( '/product/create' ) }}" ><input type="button" class="btn btn-primary" name="create-product" id="create-product" value="Add Product" /></a>

	<hr>
	{!! Form::open(['url' => 'delete-products']) !!}
		<input type="submit" class="btn btn-danger btn-xs" value="Delete" /><br/>
		
		<table class="table table-bordered table-hover table-striped">
			<thead>
				<th><center><input onclick="checkAll()" class="selectAll" type="checkbox" /></center></th>
				<th><center>Product Name</center></th>
				<th><center>SKU</center></th>
				<th><center>Model</center></th>
				<th><center>Size</center></th>
				<th><center>Price</center></th>
				<!--<th><center>Product Description</center></th>-->
			</thead>
			<tbody>	
				@foreach ( $products as $product )
					<tr>
						<th><center><input type="checkbox" name="products['<?php echo $product->id ?>']" class="checkbox" value="<?php echo $product->id ?>" /></center></th>
						<td><a href="{{ url( '/product', $product->id ) }}" ><div>{{ $product->product_name }}</div></a></td>
						<td><div>{{ $product->sku }}</div></td>
						<td><div>{{ $product->model }}</div></td>
						<td><div>{{ $product->size }}</div></td>
						<td><div>{{ $product->unit_price }}</div></td>
						<!--<td><div>{{ $product->product_desc }}</div></td>-->
					</tr>
				@endforeach
			</tbody>
		</table>
	{!! Form::close() !!}

@stop