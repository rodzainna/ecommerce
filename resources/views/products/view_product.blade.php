@extends('app')

@section('content')
	<h2>Product</h2>
	<button type="submit" data-toggle="modal" data-target="#update-product" class="btn btn-sm btn-primary">Edit</button>
	<button type="submit" data-toggle="modal" data-target="#delete-product" class="btn btn-sm btn-danger">Delete</button>
	

	<h3>{{ $product->product_name }}</h3>

	<hr>

	<product>
		<div><strong>SKU:</strong>   {{ $product->sku }}</div>
		<div><strong>Desc:</strong>  {{ $product->product_desc }}</div>
		<div><strong>Model:</strong> {{ $product->model }}</div>
		<div><strong>Size:</strong>  {{ $product->size }}</div>
		<div><strong>Price:</strong> {{ $product->unit_price }}</div>
		<div><strong>Category:</strong></div>
		@foreach( $category_infos as $cat_info )
				<button class="btn btn-warning btn-xs"><a href="{{ url( 'category', $cat_info['id'] ) }}" name="category[".$cat_info['id']."]" style="color:black;">{{ $cat_info['category_name'] }}</a>
	            <a href="{{ url( '/delete-pc/pid='.$product->id.'&cid='.$cat_info['id'].'' ) }}"><span class="glyphicon glyphicon-remove" style="color:gray;" aria-hidden="true"></span></a></button>
	            &nbsp;
		@endforeach
		
	</product>

	<!--Start Edit Product Modal-->
	<div id="update-product" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="text-align: center;  background-color: #337ab7; ">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 style="color: #ffffff; " class="modal-title">Edit Product Info</h4>
				</div>
			
				{!! Form::open(['url' => 'update-product/'.$product->id.'']) !!}
					<div class="col-lg-12 panel">
						<div class="panel-body">
							<div class="form-group">
								{!! Form::label( 'sku', 'SKU:' ) !!}
								{!! Form::text( 'sku', ''.$product->sku.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'product_name', 'Product Name:' ) !!}
								{!! Form::text( 'product_name', ''.$product->product_name.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'product_desc', 'Description: ' ) !!}
								{!! Form::textarea( 'product_desc', ''.$product->product_desc.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'product_category', 'Category: ' ) !!}
								<a href="#category-checkbox" data-toggle="collapse">See All Category(ies)</a>

								<br>
								<div id="category-checkbox" class="collapse">
									@foreach( $categories as $category )
										<?php $value = false; ?>
										@if( in_array( $category->id, $cat_ids ) )
											<?php $value = true; ?>
										@endif					
										{!! Form::label( 'cat['.$category->id.']', ''.$category->category_name.'', ['class="checkbox-inline"']  ) !!}
										{!! Form::checkbox( 'cat['.$category->id.']', ''.$category->id.'', $value ) !!}		
									@endforeach
								</div>
							</div>

							<div class="form-group">
								{!! Form::label( 'model', 'Model: ' ) !!}
								{!! Form::text( 'model', ''.$product->model.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'size', 'Size: ' ) !!}
								{!! Form::text( 'size', ''.$product->size.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							<div class="form-group">
								{!! Form::label( 'unit_price', 'Price: ' ) !!}
								{!! Form::text( 'unit_price', ''.$product->unit_price.'', ['class="form-control" style="width: 100%"'] ) !!}
							</div>

							{!! Form::submit( 'Update Product', ['class="btn btn-block btn-primary"'] ) !!}
						</div>
					</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- End edit Product Modal-->

	<!--Start Delete Product Modal-->
	<div id="delete-product" class="modal fade" role="dialog">
		<div class="modal-dialog modal-md">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="text-align: center; background-color: #337ab7; ">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 style="color: #ffffff; " class="modal-title">Delete Product</h4>
				</div>
				{!! Form::open(['url' => 'delete-product/'.$product->id.'']) !!}
					<div class="col-lg-12 panel">
						<div class="panel-body">
							<div class="form-group">
								<label>Are you sure you want to delete this product?</label>
							</div>
							<div class="form-group">
								{!! Form::submit( 'Delete Product', ['class="btn btn-danger"'] ) !!}
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- End Delete Product Modal-->

@stop